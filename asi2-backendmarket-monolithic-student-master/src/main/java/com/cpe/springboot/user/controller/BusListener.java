package com.cpe.springboot.user.controller;

import javax.jms.Message;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.cpe.springboot.user.model.UserDTO;

@Component
public class BusListener {

	@Autowired
    UserService uService;


    @JmsListener(destination = "RESULT_BUS_MNG", containerFactory = "connectionFactory")
    public void receiveMessage(UserDTO user, Message message) {

            System.out.println("[BUSLISTENER] [CHANNEL RESULT_BUS_MNG] RECEIVED String MSG=["+user+"]");
            uService.addUser(user);
    		
    }

    @JmsListener(destination = "RESULT_BUS_DUEL", containerFactory = "connectionFactory")
    public void receiveResult(String result, Message message) {
            System.out.println("[BUSLISTENER] [CHANNEL RESULT_BUS_DUEL] RECEIVED String MSG=["+result+"]");
            Object obj=JSONValue.parse(result);  
            JSONObject jsonObject = (JSONObject) obj;  

            String idPlayerOne = (String) jsonObject.get("idPlayerOne");
            String betOne = (String) jsonObject.get("betPlayerOne"); 
            uService.updateUserAccount(idPlayerOne, betOne);
            
            String idPlayerTwo = (String) jsonObject.get("idPlayerTwo");
            String betTwo = (String) jsonObject.get("betPlayerTwo"); 
            uService.updateUserAccount(idPlayerTwo, betTwo);
    }

}
